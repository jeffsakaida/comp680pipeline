# Define base image
FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env

# Copy project files
WORKDIR /app
COPY ["comp680.csproj", "./comp680.csproj"]

# Restore
RUN dotnet restore "./comp680.csproj"

# Copy all source code
COPY . ./

# Publish
WORKDIR /app
RUN find -type d -name bin -prune -exec rm -rf {} \; && find -type d -name obj -prune -exec rm -rf {} \;
RUN dotnet publish -c Release -o out

# Runtime
FROM mcr.microsoft.com/dotnet/core/runtime:3.1 AS runtime
#WORKDIR /bin/Debug/netcoreapp3.1/
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "comp680.dll"]
